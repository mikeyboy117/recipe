//
//  ViewController.m
//  Recipe2
//
//  Created by Michael Childs on 5/14/15.
//  Copyright (c) 2015 Michael Childs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int width = self.view.frame.size.width;
    int height = self.view.frame.size.height;
    
    
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    
    [scrollView setPagingEnabled: YES];
    scrollView.backgroundColor = [UIColor darkGrayColor];
    [scrollView setContentSize:CGSizeMake(width * 5, height)];
    
    [self.view addSubview:scrollView];
    
    
    UIScrollView* recipe1 = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    recipe1.contentSize = CGSizeMake(width, height);
    recipe1.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:recipe1];
    
    UIScrollView* recipe2 = [[UIScrollView alloc]initWithFrame:CGRectMake(width, 0, width, height)];
    [recipe2 setContentSize:CGSizeMake(width, height)];
    recipe2.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:recipe2];
    
    UIScrollView* recipe3 = [[UIScrollView alloc]initWithFrame:CGRectMake(width * 2, 0, width, height)];
    [recipe3 setContentSize:CGSizeMake(width, height)];
    recipe3.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:recipe3];
    
    UIScrollView* recipe4 = [[UIScrollView alloc]initWithFrame:CGRectMake(width * 3, 0, width, height)];
    [recipe4 setContentSize:CGSizeMake(width, height)];
    recipe4.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:recipe4];
    
    UIScrollView* recipe5 = [[UIScrollView alloc]initWithFrame:CGRectMake(width * 4, 0, width, height)];
    [recipe5 setContentSize:CGSizeMake(width, height)];
    recipe5.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:recipe5];
    
    
    

//    create recipe name labels
    
    UILabel* recipe1Lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 75)];
    recipe1Lbl.text = @"Chana Masala";
    recipe1Lbl.textColor = [UIColor whiteColor];
    recipe1Lbl.backgroundColor = [UIColor darkGrayColor];
    [recipe1Lbl setTextAlignment:NSTextAlignmentCenter];
    [recipe1 addSubview:recipe1Lbl];
    
    UILabel* recipe2Lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 75)];
    recipe2Lbl.text = @"Tika Masala";
    recipe2Lbl.textColor = [UIColor whiteColor];
    [recipe2Lbl setBackgroundColor:[UIColor darkGrayColor]];
    recipe2Lbl.textAlignment = NSTextAlignmentCenter;
    [recipe2 addSubview:recipe2Lbl];
    
    UILabel* recipe3Lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 75)];
    recipe3Lbl.text = @"Black Beans & Rice";
    recipe3Lbl.backgroundColor = [UIColor darkGrayColor];
    recipe3Lbl.textAlignment = NSTextAlignmentCenter;
    recipe3Lbl.textColor = [UIColor whiteColor];
    [recipe3 addSubview:recipe3Lbl];
    
    UILabel* recipe4Lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 75)];
    recipe4Lbl.text = @"Chicken & Curry";
    recipe4Lbl.textColor = [UIColor whiteColor];
    recipe4Lbl.backgroundColor = [UIColor darkGrayColor];
    recipe4Lbl.textAlignment = NSTextAlignmentCenter;
    [recipe4 addSubview:recipe4Lbl];
    
    UILabel* recipe5Lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 75)];
    recipe5Lbl.text = @"Chocolate Chip Cookies";
    recipe5Lbl.backgroundColor = [UIColor darkGrayColor];
    recipe5Lbl.textAlignment = NSTextAlignmentCenter;
    recipe5Lbl.textColor = [UIColor whiteColor];
    [recipe5 addSubview:recipe5Lbl];

    
    
    
//    create recipe imageViews
    
    UIImageView* recipe1Img = [[UIImageView alloc]initWithFrame:CGRectMake(0, recipe1Lbl.frame.size.height, width, 275)];
    recipe1Img.image = [UIImage imageNamed:@"chanaMasala.png"];
    recipe1Img.contentMode = UIViewContentModeScaleAspectFit;
    [recipe1 addSubview:recipe1Img];
    
    UIImageView* recipe2Img = [[UIImageView alloc]initWithFrame:CGRectMake(0, recipe2Lbl.frame.size.height, width, 275)];
    recipe2Img.image = [UIImage imageNamed:@"tikaMasala.png"];
    recipe2Img.contentMode = UIViewContentModeScaleAspectFit;
    [recipe2 addSubview:recipe2Img];
    
    UIImageView* recipe3Img = [[UIImageView alloc]initWithFrame:CGRectMake(0, recipe3Lbl.frame.size.height, width, 275)];
    recipe3Img.image = [UIImage imageNamed:@"bbRice.png"];
    recipe3Img.contentMode = UIViewContentModeScaleAspectFit;
    [recipe3 addSubview:recipe3Img];
    
    UIImageView* recipe4Img = [[UIImageView alloc]initWithFrame:CGRectMake(0, recipe4Lbl.frame.size.height, width, 275)];
    recipe4Img.image = [UIImage imageNamed:@"chickenCurry.png"];
    [recipe4 addSubview:recipe4Img];
    
    UIImageView* recipe5Img = [[UIImageView alloc]initWithFrame:CGRectMake(0, recipe5Lbl.frame.size.height, width, 275)];
    recipe5Img.image = [UIImage imageNamed:@"ccCookies.jpg"];
    [recipe5 addSubview:recipe5Img];
    
    
    
    
//    create recipe textViews
    
    UITextView* recipe1Text = [[UITextView alloc]initWithFrame:CGRectMake(20, recipe1Img.frame.size.height + 75, width - 40, height/2)];
    recipe1Text.text = @"This simplified version of an Indian favorite is a delightful way to showcase tasty chickpeas.\n\ningredients\n1 tablespoon olive oil\n1 large onion, chopped\n2 to 3 garlic cloves, minced\nTwo 15- to 16-ounce cans chickpeas, drained and rinsed\n1 to 2 teaspoons garam masala or good-quality curry powder n1/2 teaspoon turmeric\n2 teaspoons grated fresh or jarred ginger\n2 large tomatoes, diced\n1 tablespoon lemon juice\n1/4 cup minced fresh cilantro, or to taste\nSalt to taste\nHot cooked grain (rice, quinoa, or couscous), optional preparation\n\n1. Heat the oil in a wide skillet. Add the onion and sauté until translucent. Add the garlic and continue to sauté until the onion is golden.\n\n2. Add the chickpeas, garam masala, turmeric, ginger, tomatoes, lemon juice, and about 1/4 cup water. Bring to a simmer, then cook over medium-low heat for 10 minutes, stirring frequently. This should be moist and stewlike, but not soupy; add a little more water, if needed.\n\n3. Stir in the cilantro and season with salt. Serve on its own in shallow bowls or over a hot cooked grain, if desired.";
    recipe1Text.textColor = [UIColor whiteColor];
    recipe1Text.backgroundColor = [UIColor grayColor];
    [recipe1 addSubview:recipe1Text];
    
    
    UITextView* recipe2Text = [[UITextView alloc]initWithFrame:CGRectMake(20, recipe2Img.frame.size.height + 75, width - 40, height/2)];
    recipe2Text.text = @"ingredients\n\nFor the chicken:\n1 1/2 pounds boneless, skinless chicken breasts (3 to 4 breast halves total)\n1/4 cup plain whole-milk Greek-style yogurt\n2 tablespoons peanut oil\n2 teaspoons fresh lime or lemon juice\n1 large clove garlic, minced\n\nFor the sauce:\n1 tablespoon ground coriander\n1 1/2 teaspoons ground cumin\n1/2 teaspoon ground cardamom\n1/2 teaspoon ground nutmeg\n1 1/2 teaspoons paprika\n1/2 teaspoon cayenne\n1 tablespoon grated peeled fresh ginger (from 1-inch piece)\n4 tablespoons (1/2 stick) unsalted butter\n1 large white onion, finely chopped\n1 1/2 cups canned tomato purée (see Cook's Notes for a fresh-tomato alternative)\n\n3/4 cup water\n1/2 cup heavy cream or half-and-half\n1 1/4 teaspoons kosher salt\n1/2 teaspoon freshly ground black pepper\n1/2 cup chopped fresh cilantro plus additional sprigs for garnish";
    recipe2Text.textColor = [UIColor whiteColor];
    recipe2Text.backgroundColor = [UIColor grayColor];
    [recipe2 addSubview:recipe2Text];
    
    
    UITextView* recipe3Text = [[UITextView alloc]initWithFrame:CGRectMake(20, recipe3Img.frame.size.height + 75, width - 40, height/2)];
    recipe3Text.text = @"ingredients\n4 cloves garlic, peeled\n3 teaspoons salt\n1/4 pound bacon (about 6 strips), chopped\n2 tablespoons olive oil\n1 onion, finely chopped (about 1 cup)\n1 green pepper, seeded and finely chopped (about 3/4 cup)\n\n1 bay leaf\n1/4 teaspoon ground cumin\n1/2 teaspoon dried oregano\n1 1/2 cups long-grain white rice\n2 (15 1/2-ounce) cans black beans, not drained\n1 3/4 cups water\n1 tablespoon red wine vinegar\n\npreparation\n\nMash the garlic and render the bacon fat\nPut the garlic on a cutting board and sprinkle 1 teaspoon of salt over the cloves, let it sit for a few minutes, and mince it into a paste with a knife. Set aside.\n\nPlace the bacon and olive oil in a large pot and set it over medium-high heat. Sauté the bacon until it renders its fat and turns a golden brown color, about 6 minutes. Move the bacon around as it's cooking to prevent it from sticking to the bottom of the pot.\n\nSauté the vegetables and rice\n\nAdd the onion, green pepper, and garlic paste to the bacon and sauté until the vegetables are limp and translucent, about 5 minutes. Add the remaining 2 teaspoons of salt, the bay leaf, cumin, oregano, and rice and stir for 1 minute until well mixed and all the rice is coated in oil.\n\nAdd the beans, simmer, and serve\n\nAdd the beans and their liquid, along with the water and vinegar, to the pot. Cover and bring to a boil, then reduce to a simmer. Cook for 35 to 40 minutes, or until all the water has been absorbed by the rice. Allow the covered pot to sit off the heat for 5 minutes. Fluff the rice with a fork and serve.\n\ncooking notes\nIngredients\nCanned black beans\n\nIf you do not want to use the liquid from the canned black beans, just add an extra 1/2 cup of water with the drained and rinsed black beans.\n\nTechnique\nCooking the rice\nRice requires a specific amount of liquid to cook properly. Because onions and green peppers can contribute a considerable amount of liquid to a recipe, a volume measure for each is given. While the measurements are approximate, making sure the chopped vegetables are close to these amounts will ensure that the rice cooks properly.\n\nAdvance preparation\nYou can prepare the recipe in its entirety the night before with very little effect on the taste and texture of the dish. However, you will want to warm the dish before serving. This can be done in the microwave or on the stovetop. Just sprinkle about 1/4 cup of water over the rice to make sure it does not dry out when reheated.\nFor the chicken:\n1 1/2 pounds boneless, skinless chicken breasts (3 to 4 breast halves total)\n1/4 cup plain whole-milk Greek-style yogurt\n2 tablespoons peanut oil\n2 teaspoons fresh lime or lemon juice\n1 large clove garlic, minced\n\nFor the sauce:\n1 tablespoon ground coriander\n1 1/2 teaspoons ground cumin\n1/2 teaspoon ground cardamom\n1/2 teaspoon ground nutmeg\n1 1/2 teaspoons paprika\n1/2 teaspoon cayenne\n1 tablespoon grated peeled fresh ginger (from 1-inch piece)\n4 tablespoons (1/2 stick) unsalted butter\n1 large white onion, finely chopped\n1 1/2 cups canned tomato purée (see Cook's Notes for a fresh-tomato alternative)\n3/4 cup water\n1/2 cup heavy cream or half-and-half\n1 1/4 teaspoons kosher salt\n1/2 teaspoon freshly ground black pepper\n1/2 cup chopped fresh cilantro plus additional sprigs for garnish";
    recipe3Text.textColor = [UIColor whiteColor];
    recipe3Text.backgroundColor = [UIColor grayColor];
    [recipe3 addSubview:recipe3Text];
    
    
    UITextView* recipe4Text = [[UITextView alloc]initWithFrame:CGRectMake(20, recipe4Img.frame.size.height + 75, width - 40, height/2)];
    recipe4Text.text = @"ingredients\n\n1/2 stick (1/4 cup) unsalted butter\n2 medium onions, finely chopped (2 cups)\n2 large garlic cloves, finely chopped\n1 tablespoon finely chopped peeled fresh ginger\n3 tablespoons curry powder\n2 teaspoons salt\n1 teaspoon ground cumin\n1/2 teaspoon cayenne\n1 (3 1/2- to 4-pound) chicken, cut into 10 serving pieces\n1 (14.5-ounce) can diced tomatoes\n1/4 cup chopped fresh cilantro\n3/4 cup cashews (1/4 pound)\n3/4 cup plain whole-milk yogurt\n\nAccompaniment: cooked basmati or jasmine rice\n\nGarnish: chopped fresh cilantro\n\nPreparation\n\nHeat butter in a 5- to 6-quart wide heavy pot over moderately low heat until foam subsides, then cook onions, garlic, and ginger, stirring, until softened, about 5 minutes. Add curry powder, salt, cumin, and cayenne and cook, stirring, 2 minutes. Add chicken and cook, stirring to coat, 3 minutes. Add tomatoes, including juice, and cilantro and bring to a simmer, then cover and simmer gently, stirring occasionally, until chicken is cooked through, about 40 minutes. (If making ahead, see cooks' note, below.)\n\nJust before serving:\n\nPulse cashews in a food processor or electric coffee/spice grinder until very finely ground, then add to curry along with yogurt and simmer gently, uncovered, stirring, until sauce is thickened, about 5 minutes.";
    recipe4Text.textColor = [UIColor whiteColor];
    recipe4Text.backgroundColor = [UIColor grayColor];
    [recipe4 addSubview:recipe4Text];
    
    
    UITextView* recipe5Text = [[UITextView alloc]initWithFrame:CGRectMake(20, recipe2Img.frame.size.height + 75, width - 40, height/2)];
    recipe5Text.text = @"ingredients\n\nFor the chicken:\n1 1/2 pounds boneless, skinless chicken breasts (3 to 4 breast halves total)\n1/4 cup plain whole-milk Greek-style yogurt\n2 tablespoons peanut oil\n2 teaspoons fresh lime or lemon juice\n1 large clove garlic, minced\n\nFor the sauce:\n1 tablespoon ground coriander\n1 1/2 teaspoons ground cumin\n1/2 teaspoon ground cardamom\n1/2 teaspoon ground nutmeg\n1 1/2 teaspoons paprika\n1/2 teaspoon cayenne\n1 tablespoon grated peeled fresh ginger (from 1-inch piece)\n4 tablespoons (1/2 stick) unsalted butter\n1 large white onion, finely chopped\n1 1/2 cups canned tomato purée (see Cook's Notes for a fresh-tomato alternative)\n\n3/4 cup water\n1/2 cup heavy cream or half-and-half\n1 1/4 teaspoons kosher salt\n1/2 teaspoon freshly ground black pepper\n1/2 cup chopped fresh cilantro plus additional sprigs for garnish";
    recipe5Text.textColor = [UIColor whiteColor];
    recipe5Text.backgroundColor = [UIColor grayColor];
    [recipe5 addSubview:recipe5Text];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
