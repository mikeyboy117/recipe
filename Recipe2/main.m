//
//  main.m
//  Recipe2
//
//  Created by Michael Childs on 5/14/15.
//  Copyright (c) 2015 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
